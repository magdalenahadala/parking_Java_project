Informatics in Mechatronics 
Magdalena Hadała 292592
Mechatronic Engineering

The application is designed to support a distributed car park, in which one client connects to multiple servers via Java RMI. Each server could be an SBC computer (Raspberry PI type), which independently supports the functions of remembering the car and the client in a given place. Such infrastructure is far too complex for a regular parking lot, but an extension to such a system could be license plate recognition and detection of whether the car has left the parking space.