import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import interf.*;


public class Client {

    private Client() {}
    
    static ArrayList<MainInterface> stubs;
	private static Scanner scan;


	
    public static void main(String[] args) {
    	Object response;
    	
    	stubs = new ArrayList<MainInterface>();
    	

        try {
        	
        	for(String host_port : args) {
        		String[] a = host_port.split(":");
        		
        		System.out.println("port");
        		System.out.println(a[1]);
        		System.out.println("host");
        		System.out.println(a[0]);

        		
        		Registry reg = LocateRegistry.getRegistry(a[0], Integer.valueOf(a[1]));
                MainInterface interf = (MainInterface) reg.lookup("Server");
                
                String[] c = reg.list();
                for(String b : c) 
                   System.out.print(b);
                stubs.add(interf);
        	}
     
            System.out.println("Connected to: ");
        	
        	for(MainInterface stub : stubs) {
                response = (String) stub.sayHello();
                System.out.println("response: " + (String) response);

        	}
     
            
            int menu_level = 1;
            String command = "";
            scan = new Scanner(System.in);

            boolean first_time = true;
            
            while(menu_level > 0) {
            	
            	
    
	            if(command.matches("sell_lot.*")) {
	            	menu_level = 2;
	            	while(menu_level > 1) {
		                System.out.println("Lot ID: ");
		          
		                Lot newLot = new Lot();
		             
		                newLot.setId(scan.nextInt());
		                
		                System.out.println("For how many hours: ");
		                
		                newLot.setHours_reserved(scan.nextInt());

		                System.out.println("Car license plate: ");
		                
		                Car newCar = new Car();
		                newCar.setLicense_plate(scan.nextLine());
		                
		                System.out.println("Car make: ");
		                newCar.setMake(scan.nextLine());

		                System.out.println("Car model: ");
		                newCar.setModel(scan.nextLine());

		                System.out.println("Car color: ");
		                newCar.setColor(scan.nextLine());

		                System.out.println("Customer name: ");
		                
		                Customer newCustomer = new Customer();
		                newCustomer.setName(scan.nextLine());
		             
		                
		                System.out.println("Customer phone: ");
		                newCustomer.setPhone(scan.nextLine());

		                newLot.setCustomer(newCustomer);
		                newLot.setCar(newCar);
		                Date now = new Date();
		                newLot.setDate_reserved(now);
	            		
		            	for(MainInterface stub : stubs) {
		                    stub.updateLot(newLot);
		            	}

		            	menu_level = 1;
		                System.out.println("Lot sold!");

		                
	            	}
	            	
	                	
	            }	else
	            if(command.matches("clear_lot.*")) {
                	
	            	menu_level = 2;
	            	while(menu_level > 1) {
		                System.out.println("Lot ID: ");
		          
		                int newid = scan.nextInt();
		                
		            	for(MainInterface stub : stubs) {
		                    stub.clearLot(newid);
		            	}

		            	menu_level = 1;
		                System.out.println("Lot cleared!");

	            	}

	            }	else	            	
	            if(command.matches("show_lot.*")) {
	            	menu_level = 2;
	            	while(menu_level > 1) {
		                System.out.println("Lot ID: ");
		          
		                int newid = scan.nextInt();
		                response = null;
		            	for(MainInterface stub : stubs) {
		                    response = stub.showLot(newid);
		            	}
		            	
		            	if(response != null) {
		            	System.out.print( ((Lot) response).toString() );
		            	}
		            	
		            	
		            	menu_level = 1;

	            	}
                	
	            }	else	            	
	            if(command.matches("list_lots.*")) {
	                System.out.println("All lots: ");
	            	for(MainInterface stub : stubs) {
	                    response = stub.showLot();
	                    System.out.println("response: " + (String) response.toString());
	            	}

                	
	            }		else       
	            if(command.matches("list_empty.*")) {
	                System.out.println("Empty lots: ");
	            	for(MainInterface stub : stubs) {
	                    response = stub.isEmpty();
	                    if((boolean) response) {
		                    response = stub.showLot();
		                    System.out.println("response: " + (String) response.toString());
	                    	
	                    }
	            	}
                	
	            }	else
	            if(command.matches("help.*")) {
                	System.out.println("Commands available:");
                	System.out.println("help - shows this message");
                	System.out.println("exit - closes connection and exits the app");
                	System.out.println("list_lots - lists all lots connected to server");
                	System.out.println("list_empty - lists all empty lots");
                	System.out.println("show_lot - shows lot with license plate");
                	System.out.println("sell_lot - sells the parking lot");

	            }	else	            
	            if(command.matches("exit.*")) {
                    System.out.println("Closing the app. Bye!");

	            	menu_level = 0;	
	            }else if(!first_time){
	            	
                    System.out.println("Command not known");

	            	
	            }
	            first_time = false;
	            command = scan.nextLine();
            	
            }
            
        	for(MainInterface stub : stubs) {
                stub.exit();
                	
        	}

            
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }


}
