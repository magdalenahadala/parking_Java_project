package interf;

import java.io.Serializable;

public class Car implements Serializable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5549530664457895743L;
	private String license_plate;
	private String make;
	private String model;
	private String color;
	
	
	public Car(String license_plate, String make, String model, String color) {
		super();
		this.license_plate = license_plate;
		this.make = make;
		this.model = model;
		this.color = color;
	}
	
	public Car() {
		super();
		this.license_plate = "";
		this.make = "";
		this.model = "";
		this.color = "";
	}
	
	
	public boolean toBoolean() {
		if(!license_plate.isEmpty()) return true;
		return false;
		
	}
	public String toString() {
		return license_plate + " " + make;
	}

	public String getLicense_plate() {
		return license_plate;
	}

	public void setLicense_plate(String license_plate) {
		this.license_plate = license_plate;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}




}
