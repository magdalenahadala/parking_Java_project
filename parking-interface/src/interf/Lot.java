package interf;

import java.io.Serializable;
import java.util.Date;

public class Lot implements Comparable<Lot>, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 133234L;
	private Car car;
	private Customer customer;
	Date date_reserved;
	int hours_reserved;
	int id;
	
	public Lot(Car car, Customer customer, Date date_reserved, int hours_reserved, int id) {
		super();
		this.car = car;
		this.customer = customer;
		this.date_reserved = date_reserved;
		this.hours_reserved = hours_reserved;
		this.id = id;
	}

	public Lot() {
		super();
		this.car = new Car();
		this.customer = new Customer();
	}


	public boolean isFree() {
		if(date_reserved != null)
		return false;
		return true;
	}
	public String toString() {
		String ret = "Parking Lot:\n";		
		if(date_reserved != null) 
		ret += "\tDate reserved:" + date_reserved.toString();
		ret += "\tFor " + String.valueOf(hours_reserved) + " hours\n";
		ret += "\tCar: " + car.toString();
		ret += "\tCustomer: " + customer.toString();
		return ret;
		
		
	}
	
	
	public Car getCar() {
		return car;
	}
	public void setCar(Car car) {
		this.car = car;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Date getDate_reserved() {
		return date_reserved;
	}
	public void setDate_reserved(Date date_reserved) {
		this.date_reserved = date_reserved;
	}
	public int getHours_reserved() {
		return hours_reserved;
	}
	public void setHours_reserved(int hours_reserved) {
		this.hours_reserved = hours_reserved;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int compareTo(Lot o) {
		if(o.getId() == this.id)
			return 1;
		return 0;
	}


	
	
	
	

}
