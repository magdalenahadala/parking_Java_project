package interf;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MainInterface extends Remote {
	    String sayHello() throws RemoteException;
	    Lot showLot(int id) throws RemoteException;
	    Lot showLot() throws RemoteException;
	    boolean isEmpty() throws RemoteException;
	    void clearLot(int id) throws RemoteException;
	    void updateLot(Lot l) throws RemoteException;
	    void exit() throws RemoteException;

	}

	
	
