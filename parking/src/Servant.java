import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;

import interf.Car;
import interf.Customer;
import interf.Lot;
import interf.MainInterface;

public class Servant extends UnicastRemoteObject implements MainInterface, Serializable {
	
    public Lot lot;

    public Servant(String[] args) throws RemoteException {
        lot = new Lot();
        lot.setId(Integer.valueOf(args[0]));
    	System.out.println ("Lot id");

    	System.out.println (args[0]);

    }
    
    
    @Override
    public String sayHello() {
        return "Hello, world! write 'help' for additional info";
    }
        
	@Override
	public Lot showLot() throws RemoteException {
		return lot;
	}  
	@Override
	public Lot showLot(int id) throws RemoteException {
		if(id == lot.getId()) return lot;
		else return null;
	}

	@Override
	public boolean isEmpty() throws RemoteException {
		return lot.isFree();
	}

	@Override
	public void updateLot(Lot l) throws RemoteException {

		if(l.getId() == lot.getId())
		lot = l;
		
	}

	@Override
	public void exit() throws RemoteException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearLot(int id) throws RemoteException {
		if(id == lot.getId()) {
			lot.setCar(new Car());
			lot.setCustomer(new Customer());
			lot.setDate_reserved(new Date());
			lot.setHours_reserved(0);
		}

		// TODO Auto-generated method stub
		
	}
	
	
}
